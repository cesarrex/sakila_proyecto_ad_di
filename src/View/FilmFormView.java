package View;

import javax.swing.JFrame;

import javax.swing.JPanel;
import Controller.FilmController;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

public class FilmFormView extends JFrame {
	private JLabel labelTitulo;
    private JLabel labelTitle;
    private JLabel labelDescription;
    private JLabel labelReleaseDate;
    private JLabel labelLanguage;
    private JLabel labelDuration;
    private JLabel labelSave;
    private JPanel jPanel;
    private JTextField textFieldTitle;
    private JTextField textFieldDescription;
    private JTextField textFieldReleaseDate;
    private JTextField textFieldLanguage;
    private JTextField textFieldDuration;
	static boolean insert= true;
	int pagina, num_registros, posicion;
 
//Constructores
    public FilmFormView() {
        initComponents();
    }
    public FilmFormView(int pagina,int num_registros,int posicion) {
    	initComponents();
    	insert = false;
    	this.pagina=pagina;
    	this.num_registros=num_registros;
    	this.posicion=posicion;
    	FilmController filmController = new FilmController(textFieldTitle, textFieldDescription, textFieldReleaseDate, textFieldLanguage, textFieldDuration);
    	filmController.rellenar(pagina, num_registros, posicion);
    }

    private void initComponents() {
        jPanel = new JPanel();
        jPanel.setBackground(new Color(224, 255, 255));
        labelTitulo = new JLabel();
        labelTitulo.setFont(new Font("Impact", Font.PLAIN, 30));
        labelTitle = new JLabel();
        labelTitle.setFont(new Font("Impact", Font.PLAIN, 11));
        textFieldTitle = new JTextField();
        labelDescription = new JLabel();
        labelDescription.setFont(new Font("Impact", Font.PLAIN, 11));
        textFieldDescription = new JTextField();
        labelReleaseDate = new JLabel();
        labelReleaseDate.setFont(new Font("Impact", Font.PLAIN, 11));
        textFieldReleaseDate = new JTextField();
        labelLanguage = new JLabel();
        labelLanguage.setFont(new Font("Impact", Font.PLAIN, 11));
        textFieldLanguage = new JTextField();
        labelDuration = new JLabel();
        labelDuration.setFont(new Font("Impact", Font.PLAIN, 11));
        textFieldDuration = new JTextField();
        labelSave = new JLabel();
        labelSave.setIcon(new ImageIcon(FilmFormView.class.getResource("/Resources/Save.png")));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        labelTitulo.setText("Peliculas");

        labelTitle.setText("Title");

        labelDescription.setText("Description");

        labelReleaseDate.setText("Release Date");

        labelLanguage.setText("Language");

        labelDuration.setText("Duration");
        labelSave.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent save) {
            	FilmController filmController = new FilmController(textFieldTitle, textFieldDescription, textFieldReleaseDate, textFieldLanguage, textFieldDuration);
                if (insert) {
                	filmController.insertar();
        		} else {
        			filmController.modificar(pagina, num_registros, posicion);
        		}
                if (JOptionPane.showConfirmDialog(rootPane, "�Seguro que quieres guardar los cambios?", "Guardar",
        				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
        			dispose();
        		}
            }
        });

        javax.swing.GroupLayout gl_jPanel = new javax.swing.GroupLayout(jPanel);
        gl_jPanel.setHorizontalGroup(
        	gl_jPanel.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_jPanel.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(gl_jPanel.createParallelGroup(Alignment.LEADING)
        				.addComponent(labelTitulo, GroupLayout.PREFERRED_SIZE, 349, GroupLayout.PREFERRED_SIZE)
        				.addGroup(gl_jPanel.createSequentialGroup()
        					.addGroup(gl_jPanel.createParallelGroup(Alignment.LEADING)
        						.addComponent(labelTitle)
        						.addComponent(labelDescription)
        						.addComponent(labelDuration)
        						.addComponent(labelLanguage)
        						.addComponent(labelReleaseDate))
        					.addPreferredGap(ComponentPlacement.RELATED, 77, Short.MAX_VALUE)
        					.addGroup(gl_jPanel.createParallelGroup(Alignment.LEADING, false)
        						.addComponent(textFieldTitle, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
        						.addComponent(textFieldDescription, Alignment.TRAILING)
        						.addComponent(textFieldReleaseDate, Alignment.TRAILING)
        						.addComponent(textFieldLanguage, Alignment.TRAILING)
        						.addComponent(textFieldDuration, Alignment.TRAILING)))
        				.addComponent(labelSave, Alignment.TRAILING))
        			.addContainerGap())
        );
        gl_jPanel.setVerticalGroup(
        	gl_jPanel.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_jPanel.createSequentialGroup()
        			.addContainerGap()
        			.addComponent(labelTitulo, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
        			.addGap(18)
        			.addGroup(gl_jPanel.createParallelGroup(Alignment.LEADING)
        				.addGroup(gl_jPanel.createSequentialGroup()
        					.addComponent(labelTitle)
        					.addGap(42)
        					.addComponent(labelDescription)
        					.addGap(31)
        					.addComponent(labelReleaseDate)
        					.addGap(37)
        					.addComponent(labelLanguage)
        					.addGap(42)
        					.addComponent(labelDuration))
        				.addGroup(gl_jPanel.createSequentialGroup()
        					.addComponent(textFieldTitle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        					.addGap(36)
        					.addComponent(textFieldDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        					.addGap(31)
        					.addComponent(textFieldReleaseDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        					.addGap(31)
        					.addComponent(textFieldLanguage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        					.addGap(36)
        					.addComponent(textFieldDuration, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
        			.addPreferredGap(ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
        			.addComponent(labelSave)
        			.addContainerGap())
        );
        jPanel.setLayout(gl_jPanel);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pack();
    }
}
