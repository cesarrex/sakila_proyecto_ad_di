package View;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import Controller.ActorController;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.WindowConstants;

import java.awt.Color;

public class ActorFormView extends JFrame {
	private JLabel labelTitulo;
	private JLabel labelName;
	private JLabel labelLastName;
	private JLabel labelSave;
	private JPanel jPanel;
	private JTextField textFieldName;
	private JTextField textFieldLastName;
	public static boolean insert = true;
	private int pagina, num_registros, posicion;
	
//Contructores
	public ActorFormView() {
		initComponents();
	}

	public ActorFormView(int pagina, int num_registros, int posicion) {
		initComponents();
		this.pagina = pagina;
		this.num_registros = num_registros;
		this.posicion = posicion;
		this.insert = false;
		
		ActorController actorController = new ActorController(textFieldName, textFieldLastName);
		actorController.rellenar(pagina, num_registros, posicion);
	}
	
	private void initComponents() {
		jPanel = new JPanel();
		jPanel.setBackground(new Color(224, 255, 255));
		labelTitulo = new JLabel();
		labelTitulo.setFont(new Font("Impact", Font.PLAIN, 30));
		labelName = new JLabel();
		labelName.setFont(new Font("Impact", Font.PLAIN, 11));
		textFieldName = new JTextField();
		labelLastName = new JLabel();
		labelLastName.setFont(new Font("Impact", Font.PLAIN, 11));
		textFieldLastName = new JTextField();
		labelSave = new JLabel();
		labelSave.setIcon(new ImageIcon(ActorFormView.class.getResource("/Resources/Save.png")));

		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		labelTitulo.setText("Actor");

		labelName.setText("Name");

		labelLastName.setText("Last Name");
		
		labelSave.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent save) {
				ActorController actorController = new ActorController(textFieldName, textFieldLastName);
				if (insert) {
					actorController.insertar();
				} else {
					actorController.modificar(pagina, num_registros, posicion);
				}
				if (JOptionPane.showConfirmDialog(rootPane, "�Seguro que quieres guardar los cambios?", "Guardar",
        				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
        			dispose();
        		}
			}
		});
		getContentPane().setLayout(new BorderLayout(0, 0));

		GroupLayout gl_jPanel = new GroupLayout(jPanel);
		gl_jPanel.setHorizontalGroup(
			gl_jPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_jPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_jPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(labelTitulo, GroupLayout.PREFERRED_SIZE, 349, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_jPanel.createSequentialGroup()
							.addGroup(gl_jPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(labelLastName)
								.addComponent(labelName))
							.addGap(159)
							.addGroup(gl_jPanel.createParallelGroup(Alignment.TRAILING)
								.addComponent(textFieldLastName, GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)
								.addComponent(textFieldName, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)))
						.addComponent(labelSave, Alignment.TRAILING))
					.addContainerGap())
		);
		gl_jPanel.setVerticalGroup(
			gl_jPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_jPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(labelTitulo, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(gl_jPanel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_jPanel.createSequentialGroup()
							.addComponent(labelName)
							.addGap(42)
							.addComponent(labelLastName))
						.addGroup(gl_jPanel.createSequentialGroup()
							.addComponent(textFieldName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(42)
							.addComponent(textFieldLastName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED, 149, Short.MAX_VALUE)
					.addComponent(labelSave)
					.addContainerGap())
		);
		jPanel.setLayout(gl_jPanel);
		getContentPane().add(jPanel);
		pack();
	}
}