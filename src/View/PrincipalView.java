package View;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.WindowConstants;
import Controller.PrincipalController;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import java.awt.Component;

public class PrincipalView extends JFrame {
	private JComboBox<String> comboBox;
	private JLabel labelPeliculas;
	private JLabel labelEliminar;
	private JLabel labelEditar;
	private JLabel labelActores;
	private JLabel labelCategorias;
	private JLabel labelAnadir;
	private JLabel labelPrimero;
	private JLabel labelIzquierda;
	private JLabel labelDerecha;
	private JLabel labelUltimo;
	private JLabel labelTitulo;
	private JPanel panelTabla;
	private JPanel panelMenu;
	private JPanel panelOpciones;
	private JScrollPane jScrollPane;
	private JTable jTable;
	private JTextField textFieldPagina;
	private JPanel panelTitulo;
	private JPanel panelPaginar;

// Iniciar resto de componentes
	private void initComponents() {
		comboBox = new JComboBox<String>();
		textFieldPagina = new JTextField();
		labelTitulo = new JLabel();
		labelEditar = new JLabel();
		labelAnadir = new JLabel();
		panelPaginar = new JPanel();
		labelEliminar = new JLabel();
		panelOpciones = new JPanel();
		panelTabla = new JPanel();
		panelMenu = new JPanel();
		panelTitulo = new JPanel();
		jScrollPane = new JScrollPane();
		jTable = new JTable();
		labelPrimero = new JLabel();
		labelIzquierda = new JLabel();
		labelDerecha = new JLabel();
		labelUltimo = new JLabel();
		labelPeliculas = new JLabel();
		labelActores = new JLabel();
		labelCategorias = new JLabel();

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setBackground(new Color(0, 102, 102));

		panelTabla.setBackground(new Color(91, 104, 109));

		jScrollPane.setCursor(new java.awt.Cursor(Cursor.HAND_CURSOR));

		jTable.getTableHeader().setFont(new Font("Seoge UI", Font.BOLD, 12));
		jTable.getTableHeader().setOpaque(false);
		jTable.getTableHeader().setBackground(new Color(0, 0, 0));
		jTable.getTableHeader().setForeground(new Color(255, 255, 255));//Letras titulo tabla
		jTable.setRowHeight(25);
		jTable.setSelectionForeground(new Color(91, 104, 109));
		jTable.setShowVerticalLines(false);
		jTable.getTableHeader().setReorderingAllowed(false);

		jScrollPane.setViewportView(jTable);

		textFieldPagina.setEditable(false);
		textFieldPagina.setText("1");

		panelMenu.setBackground(new Color(255, 215, 0));

// Panel Menu
// Boton menu peliculas
		labelPeliculas.setIcon(new ImageIcon(PrincipalView.class.getResource("/Resources/menuFilm.png")));
		labelPeliculas.setAlignmentY(Component.TOP_ALIGNMENT);
		labelPeliculas.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent films) {
				PrincipalController principalController = new PrincipalController(jTable, textFieldPagina, labelTitulo,
						comboBox);
				labelTitulo.setText("PELICULAS");
				principalController.paginar();
			}
		});
// Boton menu actores
		labelActores.setIcon(new ImageIcon(PrincipalView.class.getResource("/Resources/menuActor.png")));
		labelActores.setAlignmentY(CENTER_ALIGNMENT);
		labelActores.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				PrincipalController principalController = new PrincipalController(jTable, textFieldPagina, labelTitulo,
						comboBox);
				labelTitulo.setText("ACTORES");
				principalController.paginar();
			}
		});
// Boton menu categorias
		labelCategorias.setIcon(new ImageIcon(PrincipalView.class.getResource("/Resources/menuCategoria.png")));
		labelCategorias.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		labelCategorias.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				PrincipalController principalController = new PrincipalController(jTable, textFieldPagina, labelTitulo,
						comboBox);
				labelTitulo.setText("CATEGORIAS");
				principalController.paginar();
			}
		});
	}

//Constructor
	public PrincipalView() {
		setMinimumSize(new Dimension(750, 420));
		initComponents();
		getContentPane().setLayout(new BorderLayout(0, 0));
		getContentPane().add(panelMenu, BorderLayout.WEST);
		getContentPane().add(panelTabla, BorderLayout.CENTER);
		panelTabla.setLayout(new BoxLayout(panelTabla, BoxLayout.X_AXIS));
		panelTabla.add(jScrollPane);
		getContentPane().add(panelOpciones, BorderLayout.EAST);
		getContentPane().add(panelTitulo, BorderLayout.NORTH);
		getContentPane().add(panelPaginar, BorderLayout.SOUTH);
		
		panelPaginar.setBackground(new Color(255, 215, 0));
		panelPaginar.add(labelPrimero);
		panelPaginar.add(labelIzquierda);
		panelPaginar.add(textFieldPagina);
		panelPaginar.add(labelDerecha);
		panelPaginar.add(labelUltimo);
		panelPaginar.add(comboBox);
//Combobox numero de registros visualizar
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "10", "20", "30", "40" }));
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent numRows) {
				PrincipalController principalController = new PrincipalController(jTable, textFieldPagina, labelTitulo,
						comboBox);
				principalController.paginar();
			}
		});

		panelTitulo.add(labelTitulo);
		panelTitulo.setBackground(new Color(255, 215, 0));
//Titulo
		labelTitulo.setBackground(new java.awt.Color(0, 255, 255));
		labelTitulo.setFont(new Font("Impact", Font.PLAIN, 40));
		labelTitulo.setText("PELICULAS");

		panelMenu.setLayout(new BoxLayout(panelMenu, BoxLayout.Y_AXIS));
		panelMenu.add(labelPeliculas);
		panelMenu.add(labelActores);
		panelMenu.add(labelCategorias);

		panelOpciones.setLayout(new BoxLayout(panelOpciones, BoxLayout.Y_AXIS));
		panelOpciones.setBackground(new Color(255, 215, 0));
		panelOpciones.add(labelAnadir);
		panelOpciones.add(labelEditar);
		panelOpciones.add(labelEliminar);

//A�adir
		labelAnadir.setIcon(new ImageIcon(PrincipalView.class.getResource("/Resources/Add.png")));
		labelAnadir.setAlignmentY(Component.CENTER_ALIGNMENT);
		labelAnadir.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent add) {
				PrincipalController principalController = new PrincipalController(jTable, textFieldPagina, labelTitulo,
						comboBox);
				principalController.insertar();
			}
		});

//Editar
		labelEditar.setIcon(new ImageIcon(PrincipalView.class.getResource("/Resources/Edit.png")));
		labelEditar.setAlignmentY(Component.CENTER_ALIGNMENT);
		labelEditar.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent edit) {
				PrincipalController principalController = new PrincipalController(jTable, textFieldPagina, labelTitulo,
						comboBox);
				int posicion = jTable.getSelectedRow();
				principalController.modificar(posicion + 1);
			}
		});

//Eliminar
		labelEliminar.setIcon(new ImageIcon(PrincipalView.class.getResource("/Resources/trash.png")));
		labelEliminar.setAlignmentY(Component.TOP_ALIGNMENT);
		labelEliminar.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent delete) {
				PrincipalController principalController = new PrincipalController(jTable, textFieldPagina, labelTitulo,
						comboBox);
				int posicion = jTable.getSelectedColumn();
				if (JOptionPane.showConfirmDialog(rootPane, "�Quiere borrar el registro?", "Borrar registro",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					principalController.borrar(posicion + 1);
					principalController.paginar();
				}
			}
		});

//Boton primero
		labelPrimero.setIcon(new ImageIcon(PrincipalView.class.getResource("/Resources/iconfinder_back_126585.png")));
		labelPrimero.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent first) {
				textFieldPagina.setText("1");
				PrincipalController principalController = new PrincipalController(jTable, textFieldPagina, labelTitulo,
						comboBox);
				principalController.paginar();
			}
		});
//Boton izquierda 
		labelIzquierda
				.setIcon(new ImageIcon(PrincipalView.class.getResource("/Resources/iconfinder_back-alt_134226.png")));
		labelIzquierda.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent left) {
				textFieldPagina.setText(Integer.parseInt(textFieldPagina.getText()) - 1 + "");
				PrincipalController principalController = new PrincipalController(jTable, textFieldPagina, labelTitulo,
						comboBox);
				principalController.paginar();
			}
		});

//Boton derecha
		labelDerecha.setIcon(new ImageIcon(PrincipalView.class.getResource("/Resources/iconfinder_go-alt_134226.png")));
		labelDerecha.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent right) {
				textFieldPagina.setText(Integer.parseInt(textFieldPagina.getText()) + 1 + "");
				PrincipalController principalController = new PrincipalController(jTable, textFieldPagina, labelTitulo,
						comboBox);
				principalController.paginar();
			}
		});

//Boton ultimo
		labelUltimo.setIcon(new ImageIcon(PrincipalView.class.getResource("/Resources/iconfinder_go_126585.png")));
		labelUltimo.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent last) {
				textFieldPagina.setText("0");
				PrincipalController principalController = new PrincipalController(jTable, textFieldPagina, labelTitulo,
						comboBox);
				principalController.paginar();
			}
		});
	}
}