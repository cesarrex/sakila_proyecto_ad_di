package View;

import java.awt.EventQueue;
import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginView login = new LoginView();
					login.setVisible(true);
					
					//PrincipalView login = new PrincipalView();
					//login.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
