package View;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import Controller.CategoryController;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;

public class CategoryFormView extends JFrame {
	private JLabel labelTitulo;
    private JLabel labelName;
    private JLabel labelSave;
    private JPanel jPanel;
    private JTextField textFieldNombre;
    public static boolean insert = true;
	private int pagina, num_registros, posicion;
   
    public CategoryFormView() {
        initComponents();
    }
    public CategoryFormView(int pagina,int num_registros,int posicion) {
    	initComponents();
    	this.pagina=pagina;
    	this.num_registros=num_registros;
    	this.posicion=posicion;
    	insert = false;
    	
    	CategoryController categoryController = new CategoryController(textFieldNombre);
    	categoryController.rellenar(pagina, num_registros, posicion);
    }
    
    private void initComponents() {

        jPanel = new JPanel();
        jPanel.setBackground(new Color(224, 255, 255));
        labelTitulo = new JLabel();
        labelTitulo.setFont(new Font("Impact", Font.PLAIN, 30));
        labelName = new JLabel();
        labelName.setFont(new Font("Impact", Font.PLAIN, 11));
        textFieldNombre = new JTextField();
        labelSave = new JLabel();
        labelSave.setIcon(new ImageIcon(CategoryFormView.class.getResource("/Resources/Save.png")));

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        labelTitulo.setText("Categorias");

        labelName.setText("Nombre");
        labelSave.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
            	CategoryController categoryController = new CategoryController(textFieldNombre);
                if (insert) {
                	categoryController.insertar();
        		} else {
        			categoryController.modificar(pagina, num_registros, posicion);
        		}
        		if (JOptionPane.showConfirmDialog(rootPane, "�Seguro que quieres guardar los cambios?", "Guardar cambios",
        				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
        			dispose();
        		}
            }
        });

        javax.swing.GroupLayout gl_jPanel = new javax.swing.GroupLayout(jPanel);
        gl_jPanel.setHorizontalGroup(
        	gl_jPanel.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_jPanel.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(gl_jPanel.createParallelGroup(Alignment.LEADING)
        				.addComponent(labelTitulo, GroupLayout.PREFERRED_SIZE, 349, GroupLayout.PREFERRED_SIZE)
        				.addGroup(gl_jPanel.createSequentialGroup()
        					.addComponent(labelName)
        					.addPreferredGap(ComponentPlacement.RELATED, 152, Short.MAX_VALUE)
        					.addComponent(textFieldNombre, GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE))
        				.addComponent(labelSave, Alignment.TRAILING))
        			.addContainerGap())
        );
        gl_jPanel.setVerticalGroup(
        	gl_jPanel.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_jPanel.createSequentialGroup()
        			.addContainerGap()
        			.addComponent(labelTitulo, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.UNRELATED)
        			.addGroup(gl_jPanel.createParallelGroup(Alignment.LEADING)
        				.addGroup(gl_jPanel.createSequentialGroup()
        					.addGap(1)
        					.addComponent(labelName))
        				.addComponent(textFieldNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addPreferredGap(ComponentPlacement.RELATED, 191, Short.MAX_VALUE)
        			.addComponent(labelSave)
        			.addContainerGap())
        );
        jPanel.setLayout(gl_jPanel);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }
}

