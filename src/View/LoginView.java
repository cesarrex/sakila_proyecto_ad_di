package View;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import Controller.LoginController;

import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class LoginView extends JFrame {

	private JPanel contentPane;
	private JTextField textUser;
	private JTextField textPass;

	/**
	 * Create the frame.
	 */
	public LoginView() {
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 564, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setResizable(false);

		JPanel Footer = new JPanel();
		Footer.setBackground(new Color(250, 128, 114));
		Footer.setBounds(0, 0, 238, 400);
		contentPane.add(Footer);
		Footer.setLayout(null);

		JLabel labelImgPass = new JLabel("");
		labelImgPass.setBounds(23, 165, 35, 30);
		Footer.add(labelImgPass);
		labelImgPass.setIcon(new ImageIcon("C:\\Users\\cesar\\Desktop\\C3s4r\\password1.png"));

		JLabel labelImgUser = new JLabel("");
		labelImgUser.setIcon(new ImageIcon("C:\\Users\\cesar\\Desktop\\C3s4r\\user1.png"));
		labelImgUser.setBounds(23, 83, 35, 30);
		Footer.add(labelImgUser);

		textPass = new JPasswordField();
		textPass.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		textPass.setBackground(new Color(253, 245, 230));
		textPass.setBounds(50, 165, 142, 30);
		Footer.add(textPass);
		textPass.setColumns(10);

		textUser = new JTextField();
		textUser.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		textUser.setBackground(new Color(255, 245, 238));
		textUser.setBounds(50, 83, 142, 30);
		Footer.add(textUser);
		textUser.setColumns(10);

		JTextPane txtpnErrorDeCredenciales = new JTextPane();
		txtpnErrorDeCredenciales.setBackground(new Color(250, 128, 114));
		txtpnErrorDeCredenciales.setEditable(false);
		txtpnErrorDeCredenciales.setForeground(new Color(0, 0, 0));
		txtpnErrorDeCredenciales.setFont(new Font("SansSerif", Font.PLAIN, 11));
		txtpnErrorDeCredenciales.setBounds(50, 212, 142, 51);
		Footer.add(txtpnErrorDeCredenciales);

		JLabel labelEntrar = new JLabel("");
		labelEntrar.setIcon(new ImageIcon("C:\\Users\\cesar\\Desktop\\C3s4r\\boton-ingresar1 (1).png"));
		labelEntrar.setBounds(81, 270, 50, 51);
		Footer.add(labelEntrar);

		// Para comprobar los datos enviados
		LoginController env = new LoginController(textUser, textPass, txtpnErrorDeCredenciales, this);
		labelEntrar.addMouseListener(env);

		JLabel labelExit = new JLabel("New label");
		labelExit.setIcon(new ImageIcon("C:\\Users\\cesar\\Desktop\\C3s4r\\exit (1).png"));
		labelExit.setBounds(524, 11, 30, 30);
		contentPane.add(labelExit);

		labelExit.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});

		JLabel labelFilmpng = new JLabel("New label");
		labelFilmpng.setIcon(new ImageIcon(LoginView.class.getResource("/Resources/pngfuel.png")));
		labelFilmpng.setVerticalAlignment(SwingConstants.BOTTOM);
		labelFilmpng.setHorizontalAlignment(SwingConstants.CENTER);
		labelFilmpng.setBounds(237, -66, 337, 423);
		contentPane.add(labelFilmpng);

		JLabel labelFondoImagen = new JLabel("New label");
		labelFondoImagen.setVerticalAlignment(SwingConstants.BOTTOM);
		labelFondoImagen.setHorizontalAlignment(SwingConstants.CENTER);
		labelFondoImagen.setIcon(new ImageIcon(LoginView.class.getResource("/Resources/wallpaper.jpg")));
		labelFondoImagen.setBounds(238, 0, 326, 400);
		contentPane.add(labelFondoImagen);

	}
}
