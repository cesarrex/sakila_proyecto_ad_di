package Model;

import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import Singleton.Singleton;

public class CategoryModel {
	private int id_categoria;
	private String nombre;
	private Statement st;
	private ResultSet rs;
	
//Contructor
	public CategoryModel() {
		try {
			Connection conexion = (Connection) Singleton.getConnection();
			st = (Statement) conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public CategoryModel(int id_categoria, String nombre) {
		this.setId(id_categoria);
		this.setNombre(nombre);
		try {
			Connection conexion = (Connection) Singleton.getConnection();
			st = (Statement) conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//Getters y setters
	public int getId() {
		return id_categoria;
	}

	public void setId(int id) {
		this.id_categoria = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
//Metodo para obtener registro y rellenar el form
	public CategoryModel obtenerRegistro(int pagina, int num_registros, int posicion) {
		CategoryModel categoryModel = new CategoryModel();
		try {
			rs = st.executeQuery("SELECT category_id, name FROM category");
			rs.absolute((pagina-1)*num_registros);
			rs.relative(posicion);
			categoryModel.setId(rs.getInt(1));
			categoryModel.setNombre(rs.getString(2));
		
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return categoryModel;
	}
	
//Metodo para modificar categoria
	public void modificarCategory(int pagina, int num_registros, int posicion) {
		try {
			rs = st.executeQuery("SELECT * FROM category");
			rs.absolute((pagina-1)*num_registros);
			rs.relative(posicion);
			rs.updateString("name", getNombre());
			rs.updateRow();

			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//Metodo para borrar categoria
	public void borrarCategory(int pagina, int num_registros, int posicion) {
		try {
			rs = st.executeQuery("SELECT * FROM category");
			rs.absolute((pagina-1)*num_registros);
			rs.relative(posicion);
			
			if(rs.next()) {
				rs.deleteRow();
			}
			
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//Insertar categoria
	public void insertarCategory() {	
		try {
			rs = st.executeQuery("SELECT * FROM category");
			rs.moveToInsertRow();
			rs.updateString("name", getNombre());
			rs.insertRow();
			rs.moveToCurrentRow();

			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//Obtener categorias
	public ArrayList<CategoryModel> obtener(int pagina, int num_registros) {
		ArrayList<CategoryModel> categoryModel = new ArrayList<CategoryModel>();
		try {
			rs = st.executeQuery("SELECT category_id, name FROM category");
			rs.absolute((pagina-1)*num_registros);
			int resultados=0;
			while (resultados!=num_registros && rs.next()) {
				categoryModel.add(new CategoryModel(rs.getInt(1), rs.getString(2)));
				resultados++;
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return categoryModel;
	}
}
