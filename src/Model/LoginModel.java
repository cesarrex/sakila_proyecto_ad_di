package Model;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Libs.JSONArray;
import Libs.JSONObject;
import Libs.ParameterStringBuilder;
import View.PrincipalView;

public class LoginModel {
	private String user;
	private String password;
	private StringBuffer content;

	public LoginModel(String user, String password) {
		this.user = user;
		this.password = password;
	}

	public String JsonParser() {
		String jsonStr = this.content + "";
		JSONObject json = new JSONObject(jsonStr);
		System.out.println(json.toString());
		String brand = (String) json.get("msg");
		return (brand);
	}

	public boolean enviar() throws Exception {
		URL url = new URL("https://dam.inasoft.es/login.php");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		Map<String, String> parameters = new HashMap<>();
		parameters.put("user", this.user);
		parameters.put("password", this.password);
		con.setDoOutput(true);
		DataOutputStream out = new DataOutputStream(con.getOutputStream());
		out.writeBytes(ParameterStringBuilder.getParamsString(parameters));
		int status = con.getResponseCode();
		System.out.println(status);
		out.flush();
		out.close();

		// Comprobar Errores

		BufferedReader in;
		if (status > 299) {
			in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		} else {
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			return true;
		}
		
		//Hacemos un StringBuffer para enviar el mensaje JSON al login
		String inputLine;
		this.content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}

		// Cerramos la conexion
		in.close();
		con.disconnect();
		return false;
	}
}
