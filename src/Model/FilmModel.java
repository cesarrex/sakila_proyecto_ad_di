package Model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import Singleton.Singleton;

public class FilmModel {
	private int id_film;
	private String titulo;
	private String descripcion;
	private int lanzamiento;
	private int lenguaje;
	private int duracion;
	private Statement st;
	private ResultSet rs;
	
//Contructor
	public FilmModel () {
		try {
			Connection conexion = (Connection) Singleton.getConnection();
			st = (Statement) conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public FilmModel(int id_film, String titulo, String descripcion, int lanzamiento, int lenguaje, int duracion) {
		this.setId_film(id_film);
		this.setTitulo(titulo);
		this.setDescripcion(descripcion);
		this.setLanzamiento(lanzamiento);
		this.setLenguaje(lenguaje);
		this.setDuracion(duracion);
		try {
			Connection conexion = (Connection) Singleton.getConnection();
			st = (Statement) conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

//Getters y setters
	public int getId_film() {
		return id_film;
	}

	public void setId_film(int id_film) {
		this.id_film = id_film;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getLanzamiento() {
		return lanzamiento;
	}

	public void setLanzamiento(int lanzamiento) {
		this.lanzamiento = lanzamiento;
	}

	public int getLenguaje() {
		return lenguaje;
	}

	public void setLenguaje(int lenguaje) {
		this.lenguaje = lenguaje;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	
//Metodo para obtener registro y rellenar el form
	public FilmModel obtenerRegistro(int pagina, int num_registros, int posicion) {
		FilmModel filmModel = new FilmModel();
		try {
			rs = st.executeQuery("SELECT film_id, title, description, release_year, language_id,length FROM film");
			rs.absolute((pagina-1)*num_registros);
			rs.relative(posicion);
			filmModel.setId_film(rs.getInt(1));
			filmModel.setTitulo(rs.getString(2));
			filmModel.setDescripcion(rs.getString(3));
			filmModel.setLanzamiento(rs.getInt(4));
			filmModel.setLenguaje(rs.getInt(5));
			filmModel.setDuracion(rs.getInt(6));
			
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return filmModel;
	}

//Metodo modificar pelicula
	public void modificarFilm(int pagina, int num_registros, int posicion) {
		try {
			rs = st.executeQuery("SELECT * FROM film");
			rs.absolute((pagina-1)*num_registros);
			rs.relative(posicion-1);
			rs.next();
			rs.updateString("title", getTitulo());
			rs.updateString("description", getDescripcion());
			rs.updateInt("release_year", getLanzamiento());
			rs.updateInt("language_id", getLenguaje());
			rs.updateInt("length", getDuracion());
			System.out.println(rs.getRow());
			rs.updateRow();

			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//Metodo borrar pelicula
	public void borrarFilm(int pagina, int num_registros, int posicion) {
		try {
			rs = st.executeQuery("SELECT * FROM film");
			rs.absolute((pagina-1)*num_registros);
			rs.relative(posicion-1);
			
			if(rs.next()) {
				rs.deleteRow();
			}
			
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//Metodo insertar pelicula
	public void insertarFilm() {
		try {
			rs = st.executeQuery("SELECT * FROM film");
			rs.moveToInsertRow();
			rs.updateString("title", getTitulo());
			rs.updateString("description", getDescripcion());
			rs.updateInt("release_year", getLanzamiento());
			rs.updateInt("language_id", getLenguaje());
			rs.updateInt("length", getDuracion());
			rs.insertRow();
			rs.moveToCurrentRow();

			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//Metodo obtener peliculas
	public ArrayList<FilmModel> obtener(int pagina, int num_registros) {
		ArrayList<FilmModel> filmModel = new ArrayList<FilmModel>();
		try {
			rs = st.executeQuery("SELECT film_id, title, description, release_year, language_id,length FROM film");
			rs.absolute((pagina-1)*num_registros);
			int resultados=0;
			while (resultados!=num_registros && rs.next()) {
				filmModel.add(new FilmModel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5),
						rs.getInt(6)));
				resultados++;
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return filmModel;
	}
}