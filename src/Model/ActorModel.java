package Model;

import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Singleton.Singleton;

public class ActorModel {
	private int id_actor;
	private String nombre;
	private String apellido;
	private Statement st;
	private ResultSet rs;
	
	public ActorModel() {
		try {
			Connection conexion = (Connection) Singleton.getConnection();
			st = (Statement) conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ActorModel(int id_actor, String nombre, String apellido) {
		this.setId(id_actor);
		this.setNombre(nombre);
		this.setApellido(apellido);
		try {
			Connection conexion = (Connection) Singleton.getConnection();
			st = (Statement) conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//Getters y setters
	public int getId() {
		return this.id_actor;
	}

	public void setId(int id) {
		this.id_actor = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
//Metodo para obterner registro para rellenar el form
	public ActorModel obtenerRegistro(int pagina, int num_registros, int posicion) {
		ActorModel actorModel = new ActorModel();
		try {
			rs = st.executeQuery("SELECT actor_id, first_name, last_name FROM actor");
			rs.absolute((pagina-1)*num_registros);
			rs.relative(posicion);
			actorModel.setId(rs.getInt(1));

			actorModel.setNombre(rs.getString(2));
			actorModel.setApellido(rs.getString(3));
		
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return actorModel;
	}
	
//Metodo para modificar actor
	public void modificarActor(int pagina, int num_registros, int posicion) {
		try {
			rs = st.executeQuery("SELECT * FROM actor");
			rs.absolute((pagina-1)*num_registros);
			rs.relative(posicion-1);
			rs.next();
			rs.updateString("first_name", getNombre());
			rs.updateString("last_name", getApellido());
			rs.updateRow();
			rs.moveToCurrentRow();
			
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//Metodo para borrar actor
	public void borrarActor(int pagina, int num_registros, int posicion) {
		try {
			rs = st.executeQuery("SELECT * FROM actor");
			rs.absolute((pagina-1)*num_registros);
			rs.relative(posicion-1);
			if(rs.next()) {
				rs.deleteRow();
			}
			
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//Metodo para insertar actor
	public void insertarActor() {	
		try {
			rs = st.executeQuery("SELECT * FROM actor");
			rs.moveToInsertRow();
			rs.updateString("first_name", getNombre());
			rs.updateString("last_name", getApellido());
			rs.insertRow();
			rs.moveToCurrentRow();

			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//Metodo obtener peliculas para la tabla
	public ArrayList<ActorModel> obtener(int pagina, int num_registros) {
		ArrayList<ActorModel> actorModel = new ArrayList<ActorModel>();
		try {
			rs = st.executeQuery("SELECT actor_id, first_name, last_name FROM actor");
			rs.absolute((pagina-1)*num_registros);
			int resultados=0;
			while (resultados!=num_registros && rs.next()) {
				actorModel.add(new ActorModel(rs.getInt(1), rs.getString(2), rs.getString(3)));
				resultados++;
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return actorModel;
	}
}
