package Singleton;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Singleton {
	/**
	 * Conexi�n �nica de BD
	 */
	private static Connection conn = null;
	/**
	 * Driver de conexi�n a BD
	 */
	private String driver;
	/**
	 * URL de conexi�n a BD
	 */
	private String url;
	/**
	 * Usuario de BD
	 */
	private String usuario;
	/**
	 * Clave de BD
	 */
	private String password;

	/**
	 * Constructor Inicializa par�metros de conexi�n y crea la conexi�n
	 *
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private Singleton() throws ClassNotFoundException, SQLException {
		String url = "jdbc:mysql://localhost:3306/sakila";
		String driver = "com.mysql.cj.jdbc.Driver";
		String usuario = "root";
		String password = "";

		Class.forName(driver);
		conn = DriverManager.getConnection(url, usuario, password);
	}

	/**
	 * Obtiene la conexi�n, si no existe la crea.
	 *
	 * @return Conexi�n creada
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		if (conn == null) {
			new Singleton();
		}

		return conn;
	}

	public static void main(String[] args) {
		try {
			Connection conexion = (Connection) Singleton.getConnection();
			if (conexion != null) {
				System.out.println("Conexi�n a base de datos OK");
				Statement st = conexion.createStatement();
				ResultSet rs = st.executeQuery("select * from film");
				while (rs.next()) {
					System.out.println(rs.getInt("film_id"));
				}
				rs.close();
				st.close();
			}
			conexion.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
