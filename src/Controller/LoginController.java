package Controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import Model.LoginModel;
import View.PrincipalView;
import View.LoginView;

public class LoginController implements MouseListener {
	private JTextField user;
	private JTextField password;
	private JTextPane txtpnErrorDeCredenciales;
	private LoginView login;
	
	public LoginController (JTextField user, JTextField password, JTextPane txtpnErrorDeCredenciales, LoginView login) {
		this.user=user;
		this.password=password;
		this.txtpnErrorDeCredenciales=txtpnErrorDeCredenciales;
		this.login=login;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		String usuario=user.getText();
		String pass=password.getText();
		
		try {
			LoginModel loginmodel=new LoginModel(usuario,pass);
			if(loginmodel.enviar()==true) { 
				//Iniciamos la ventana menu
				PrincipalView menu = new PrincipalView();
				menu.setVisible(true);
				//Cerramos la ventana login
				//login.dispatchEvent(new WindowEvent(login, WindowEvent.WINDOW_CLOSING));
				login.dispose();
			}else {
				txtpnErrorDeCredenciales.setText(loginmodel.JsonParser()+"");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}