package Controller;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;

import Model.ActorModel;
import Model.CategoryModel;
import Model.FilmModel;
import View.ActorFormView;
import View.CategoryFormView;
import View.FilmFormView;

public class PrincipalController {
	private JTable tabla;
	private JTextField pagina;
	private JLabel titulo;
	private JComboBox registrosPorPagina;
	private int IntRegistros;
	private int IntPagina;

//Constructor
	public PrincipalController(JTable tabla, JTextField pagina, JLabel titulo, JComboBox registrosPorPagina) {
		this.tabla = tabla;
		this.pagina = pagina;
		this.titulo = titulo;
		this.registrosPorPagina = registrosPorPagina;

	}

//Metodo paginar
	public void paginar() {
		String o = String.valueOf(registrosPorPagina.getSelectedItem());
		IntRegistros = Integer.parseInt(o);
		IntPagina = Integer.parseInt(pagina.getText());
		switch (titulo.getText()) {
		case "PELICULAS":
			this.rellenarPeliculas();
			break;
		case "ACTORES":
			this.rellenarActores();
			break;
		case "CATEGORIAS":
			this.rellenarCategorias();
			break;
		default:
			break;
		}
	}

//Metodo para rellenar las categorias
	public void rellenarCategorias() {
		CategoryModel cm = new CategoryModel(0, "");
		Object[][] datos = new Object[IntRegistros][4];
		ArrayList<CategoryModel> am = cm.obtener(IntPagina, IntRegistros);
		int i = 0;
		for (CategoryModel categoryModel : am) {
			datos[i][0] = categoryModel.getId();
			datos[i][1] = categoryModel.getNombre();
			i++;
		}
		String[] titulos = new String[] { "ID", "Name" };
		tabla.setModel(new javax.swing.table.DefaultTableModel(datos, titulos));
		titulo.setText("CATEGORIAS");
	}

//Metodo para rellenar Actores
	public void rellenarActores() {
		ActorModel actorModel = new ActorModel(0, "", "");
		Object[][] datos = new Object[IntRegistros][3];
		ArrayList<ActorModel> arrayActorModel = actorModel.obtener(IntPagina, IntRegistros);
		int i = 0;
		for (ActorModel actorModelFor : arrayActorModel) {
			datos[i][0] = actorModelFor.getId();
			datos[i][1] = actorModelFor.getNombre();
			datos[i][2] = actorModelFor.getApellido();
			i++;
		}
		titulo.setText("ACTORES");
		String[] titulos = new String[] { "ID", "Name", "Last Name" };
		tabla.setModel(new javax.swing.table.DefaultTableModel(datos, titulos));
	}

//Metodo para rellenar Peliculas
	public void rellenarPeliculas() {
		FilmModel filmModel = new FilmModel(0, "", "", 0, 0, 0);
		Object[][] datos = new Object[IntRegistros][6];
		ArrayList<FilmModel> arrayFilmModel = filmModel.obtener(IntPagina, IntRegistros);
		int i = 0;
		for (FilmModel filmModelFor : arrayFilmModel) {
			datos[i][0] = filmModelFor.getId_film();
			datos[i][1] = filmModelFor.getTitulo();
			datos[i][2] = filmModelFor.getDescripcion();
			datos[i][3] = filmModelFor.getLanzamiento();
			datos[i][4] = filmModelFor.getLenguaje();
			datos[i][5] = filmModelFor.getDuracion();
			i++;
		}
		titulo.setText("PELICULAS");
		String[] titulos = new String[] { "ID", "Title", "Description", "Release Year", "Language", "Duration" };
		tabla.setModel(new javax.swing.table.DefaultTableModel(datos, titulos));
	}

// Metodo para insertar
	public void insertar() {
		switch (titulo.getText()) {
		case "PELICULAS":
			new FilmFormView().setVisible(true);
			break;
		case "ACTORES":
			new ActorFormView().setVisible(true);
			break;
		case "CATEGORIAS":
			new CategoryFormView().setVisible(true);
			break;
		default:
			break;
		}
	}

// Metodo para borrar
	public void borrar(int posicion) {
		String tabla = titulo.getText();
		String stringRegistros = String.valueOf(registrosPorPagina.getSelectedItem());
		IntRegistros = Integer.parseInt(stringRegistros);
		IntPagina = Integer.parseInt(pagina.getText());
		switch (tabla) {
		case "PELICULAS":
			new FilmModel().borrarFilm(IntPagina, IntRegistros, posicion);
			break;
		case "ACTORES":
			new ActorModel().borrarActor(IntPagina, IntRegistros, posicion);
			break;
		case "CATEGORIAS":
			new CategoryModel().borrarCategory(IntPagina, IntRegistros, posicion);
			break;
		default:
			break;
		}
	}

// Metodo para modificar dependiendo de la tabla
	public void modificar(int posicion) {
		String tabla = titulo.getText();
		String stringRegistros = String.valueOf(registrosPorPagina.getSelectedItem());
		IntRegistros = Integer.parseInt(stringRegistros);
		IntPagina = Integer.parseInt(pagina.getText());
		switch (tabla) {
		case "PELICULAS":
			new FilmFormView(IntPagina, IntRegistros, posicion).setVisible(true);
			break;
		case "ACTORES":
			new ActorFormView(IntPagina, IntRegistros, posicion).setVisible(true);
			break;
		case "CATEGORIAS":
			new CategoryFormView(IntPagina, IntRegistros, posicion).setVisible(true);
			break;
		default:
			break;
		}
	}
}
