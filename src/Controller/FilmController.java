package Controller;

import javax.swing.JTextField;
import Model.FilmModel;

public class FilmController {

	private JTextField textFieldTitulo;
	private JTextField textFieldDescripcion;
	private JTextField textFieldLanzamiento;
	private JTextField textFieldLenguaje;
	private JTextField textFieldDuracion;

//Constructor
	public FilmController(JTextField textFieldTitulo, JTextField textFieldDescripcion, JTextField textFieldLanzamiento,
			JTextField textFieldLenguaje, JTextField textFieldDuracion) {
		this.textFieldTitulo = textFieldTitulo;
		this.textFieldDescripcion = textFieldDescripcion;
		this.textFieldLanzamiento = textFieldLanzamiento;
		this.textFieldLenguaje = textFieldLenguaje;
		this.textFieldDuracion = textFieldDuracion;
	}

// Metodo rellenar datos form pelicula
	public void rellenar(int pagina, int num_registros, int posicion) {
		FilmModel filmModel = new FilmModel();
		filmModel = filmModel.obtenerRegistro(pagina, num_registros, posicion);
		this.textFieldTitulo.setText("" + filmModel.getTitulo());
		this.textFieldDescripcion.setText("" + filmModel.getDescripcion());
		this.textFieldLanzamiento.setText("" + filmModel.getLanzamiento());
		this.textFieldLenguaje.setText("" + filmModel.getLenguaje());
		this.textFieldDuracion.setText("" + filmModel.getDuracion());
	}

//Metodo insertar pelicula
	public void insertar() {
		String titulo = this.textFieldTitulo.getText();
		String descripcion = this.textFieldDescripcion.getText();
		int lanzamiento = Integer.parseInt(this.textFieldLanzamiento.getText());
		int lenguaje = Integer.parseInt(this.textFieldLenguaje.getText());
		int duracion = Integer.parseInt(this.textFieldDuracion.getText());
		FilmModel filmModel = new FilmModel(1, titulo, descripcion, lanzamiento, lenguaje, duracion);
		filmModel.insertarFilm();
	}

//Metodo modificar pelicula
	public void modificar(int pagina, int num_registros, int posicion) {
		String titulo = this.textFieldTitulo.getText();
		String descripcion = this.textFieldDescripcion.getText();
		int anoLanzamiento = Integer.parseInt(this.textFieldLanzamiento.getText());
		int lenguaje = Integer.parseInt(this.textFieldLenguaje.getText());
		int duracion = Integer.parseInt(this.textFieldDuracion.getText());
		FilmModel filmModel = new FilmModel(1, titulo, descripcion, anoLanzamiento, lenguaje, duracion);
		filmModel.modificarFilm(pagina, num_registros, posicion);
	}
}