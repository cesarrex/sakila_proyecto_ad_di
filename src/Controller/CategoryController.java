package Controller;

import javax.swing.JTextField;
import Model.CategoryModel;

public class CategoryController {
    private JTextField textFieldNombre;
    
//Constructor
	public CategoryController(JTextField textFieldNombre) {
		this.textFieldNombre = textFieldNombre;
	}

//Metodo para rellenar el form
	public void rellenar(int pagina,int num_registros,int posicion) {
		CategoryModel categoryModel=new CategoryModel();
		categoryModel = categoryModel.obtenerRegistro(pagina, num_registros, posicion);
		this.textFieldNombre.setText(""+categoryModel.getNombre());
		
	}

//Metodo para modificar
	public void modificar(int pagina,int num_registros,int posicion) {
		String titulo= this.textFieldNombre.getText();

		CategoryModel categoryModel= new CategoryModel(1, titulo);
		categoryModel.modificarCategory(pagina, num_registros, posicion);
	}
	
//Metodo para insertar
	public void insertar() {
		String titulo= this.textFieldNombre.getText();

		CategoryModel categoryModel= new CategoryModel(1, titulo);
		categoryModel.insertarCategory();
	}
}
