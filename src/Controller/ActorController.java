package Controller;


import javax.swing.JTextField;
import Model.ActorModel;

public class ActorController {
    private JTextField textFieldNombre;
    private JTextField textfieldApellido;
    
//Contructor
	public ActorController(JTextField textFieldNombre, JTextField textfieldApellido) {
		this.textFieldNombre = textFieldNombre;
		this.textfieldApellido = textfieldApellido;
	}
	
//Metodo para modificar
	public void modificar(int pagina,int num_registros,int posicion) {
		String nombre= textFieldNombre.getText();
		String apellido= textfieldApellido.getText();

		ActorModel actorModel= new ActorModel(1, nombre, apellido);
		actorModel.modificarActor(pagina, num_registros, posicion);
	}
	
//Metodo para rellenar el form
	public void rellenar(int pagina,int num_registros,int posicion) {
		ActorModel actorModel=new ActorModel();
		actorModel = actorModel.obtenerRegistro(pagina, num_registros, posicion);
		textFieldNombre.setText(""+actorModel.getNombre());
		textfieldApellido.setText(""+actorModel.getApellido());
	}
	
//Metodo insertar	
	public void insertar() {
		String nombre= textFieldNombre.getText();
		String apellido= textfieldApellido.getText();

		ActorModel actorModel= new ActorModel(1, nombre, apellido);
		actorModel.insertarActor();
	}
}